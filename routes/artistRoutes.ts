import { Request, Response, Router } from 'express';

//modelos
import { Artist } from '../models/Artist';
import { Album } from '../models/Album';
import { Song } from '../models/Song';

//middleware autenticación
import { validateToken } from '../middlewares/validateToken';


//fileSystem
import fs from 'fs';
import path from 'path';


//multipart
var multipart = require('connect-multiparty');
var md_upload = multipart({ uploadDir: './uploads/artists' });



export var artistRoutes = Router();

//crear un nuevo artista
artistRoutes.post('/create', validateToken, (req: Request, res: Response) => {

    var artist = {
        name: req.body.name,
        description: req.body.description,
        image: req.body.image || 'default_artist.jpg',
    };

    if (!artist.name) {
        return res.status(400).json({
            ok: false,
            message: 'El nombre del artista es obligaorio',
        });
    }
    Artist.create(artist).then(artistCreated => {
        if (artistCreated) {
            res.status(200).json({
                ok: true,
                message: 'nuevo artista creado con éxito',
                artist: artistCreated,
            });
        } else {
            res.status(400).json({
                ok: false,
                message: 'Error al crear nuevo artista',
            });
        }
    })
        .catch(error => {
            res.status(500).json({
                ok: false,
                error,
            });
        });
});

//conseguir artista por Id

artistRoutes.get('/get/:artistId', (req: Request, res: Response) => {
    var artistId = req.params.artistId;
    if (!artistId) {
        return res.status(400).json({
            ok: false,
            message: 'Falta el id del artista',
        });
    }

    Artist.findById(artistId, (err, artist) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                message: 'Error en la petición de artista',
            });
        }
        if (artist) {
            res.status(200).json({
                ok: true,
                artist,
            })
        } else {
            res.status(400).json({
                ok: false,
                message: 'No existe el artista',
            })
        }
    });
});


//listado paginado de artistas
artistRoutes.get('/getAll/:page', (req: Request, res: Response) => {
    var page = req.params.page;
    var itemsPerPage = 5;
    Artist.paginate({}, { sort: 'name', page: page, perPage: itemsPerPage }, (err, result) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                message: 'Error en la petición',
            });
        }
        if (result) {
            return res.status(200).json({
                ok: true,
                result,
            });
        } else {
            return res.status(400).json({
                ok: true,
                error: 'No hay resultados',
            });
        }
    });

});

//actualizar artista
artistRoutes.post('/update/:artistId', validateToken, (req: any, res: Response) => {

    var artistId = req.params.artistId;

    var artist = req.body;

    Artist.findByIdAndUpdate(artistId, artist, { new: true }, (error, artistUpdated) => {
        if (error) {
            return res.status(500).json({
                ok: false,
                message: 'Error al actualizar artista',
                error,
            });
        }
        if (artistUpdated) {
            res.status(200).json({
                ok: true,
                message: 'Artista actualizado con éxito',
                artist: artistUpdated,
            });
        } else {
            res.status(400).json({
                ok: false,
                message: 'No se puede actualizar el artista, parece que no existe',
            });
        }
    });
});

//Eliminar un artista
artistRoutes.delete('/delete/:artistId', [validateToken,md_upload], (req: Request, res: Response) => {

    var artistId = req.params.artistId;

    //borramos el arista y después los álbumes y canciiones relacionas mediante promesas
    Artist.findByIdAndDelete(artistId)
        .then(artistDeleted => {
            Album.find({ artist: artistDeleted._id }).remove()
                .then(albumDeleted => {
                    Song.find({ album: albumDeleted._id }).remove()
                    return res.status(200).json({
                        ok:true,
                        message:'Artista borrado con éxito',
                    })
                })
        }).catch(error => {
            return res.status(400).json({
                ok:false,
                error,
            });
        });
});


//Subir imagen de artista
artistRoutes.post('/upload-image/:artistId',[validateToken,md_upload],(req:any,res:Response)=>{

    var artistId=req.params.artistId;

    if(req.files){
        var file_path:string=req.files.image.path;
        var file_path_split=file_path.split('\\');
        console.log(file_path);
        var file_name=file_path_split[2];
        var file_name_split=file_name.split('\.');
        var file_extension=file_name_split[1];
        if(file_extension=='jpg' || file_extension=='jpeg' || file_extension=='png' || file_extension=='gif'){
            Artist.findByIdAndUpdate(artistId,{image:file_name},{new:true},(error,artistUpdated)=>{
                if(error){
                    return res.status(500).json({
                        ok:false,
                        error,
                    });
                }
                if(artistUpdated){
                    return res.status(200).json({
                        ok:true,
                        message:'Imagen subida con éxito',
                        artista:artistUpdated,
                    });
                }else{
                    return res.status(400).json({
                        ok:false,
                        message:'Error al actualizar el artista',
                    });
                }
            });
        }else{
            fs.unlink(file_path,(err)=>{
                if(err){
                    console.log(err);
                }
            });
            return res.status(400).json({
                ok:false,
                message:'El formato del archivo de imagen no es correcto',
            });
        }
       
    }else{
        res.status(404).json({
            ok:false,
            message:'no has subido ninguna imagen',
        });
    }
});

//conseguir imagen de artista con su Id
artistRoutes.get('/get-image/:artistId',(req:Request,res:Response)=>{
    var artistId=req.params.artistId;

    Artist.findById(artistId,(err,artist)=>{
        if(err){
            return res.status(500).json({
                ok:false,
                message:'Error en la petición de imagen',
            });
        }
        if(artist){
            var pathFile='./uploads/artists/'+artist.image;
            fs.exists(pathFile,(exists)=>{
                if(exists){
                    return res.status(200).sendFile(path.resolve(pathFile));
                }else{
                    return res.status(404).json({
                        ok:false,
                        message:'No existe imagen de artista',
                    });
                }
            });
        }else{
            res.status(400).json({
                ok:true,
                message:'No existe el artista',
            });
        }
    });
});





