import { Request, Response, Router } from 'express';

//modelos
import { Artist } from '../models/Artist';
import { Album } from '../models/Album';
import { Song } from '../models/Song';

//middleware autenticación
import { validateToken } from '../middlewares/validateToken';

//fileSystem
import fs from 'fs';
import path from 'path';

//multipart
var multipart = require('connect-multiparty');
var md_upload = multipart({ uploadDir: './uploads/albums' });

export var albumRoutes = Router();

//Crear nuevo álbum
albumRoutes.post('/create', validateToken, (req: Request, res: Response) => {
    var album = {
        title: req.body.title,
        year: req.body.year || null,
        image: req.body.image || 'default_album.jpg',
        description: req.body.description || null,
        artist: req.body.artist,
    };

    if (!album.title || !album.artist) {
        return res.status(400).json({
            ok: false,
            message: 'Faltan datos, rellena los campos obligatorios',
        });
    }

    Album.create(album).then(albumCreated => {
        if (albumCreated) {
            res.status(200).json({
                ok: true,
                message: 'Nuevo album creado con éxito',
                album: albumCreated,
            });
        } else {
            res.status(500).json({
                ok: false,
                message: 'Error al crear nuevo álbum',
            });
        }
    })
        .catch(error => {
            res.status(500).json({
                ok: false,
                error,
            });
        });
});

//Conseguir un álbum por su id
albumRoutes.get('/get/:albumId', (req: Request, res: Response) => {
    var albumId = req.params.albumId;

    Album.findById(albumId).populate({ path: 'artist' }).exec((error, album) => {
        if (error) {
            return res.status(500).json({
                ok: false,
                message: 'Error en la petición de álbum',
                error,
            });
        }
        if (album) {
            res.status(200).json({
                ok: true,
                album,
            });
        } else {
            res.status(400).json({
                ok: false,
                message: 'No existe el álbum',
            });
        }
    });
});

//Conseguir albums
albumRoutes.get('/getAll/:artistId?', validateToken, (req: Request, res: Response) => {
    var artistId = req.params.artistId;

    if (!artistId) {
        //si no hay parametro de id artista sacará todos los albums
        Album.find({}).populate({ path: 'artist' }).exec((error, albums) => {
            if (error) {
                return res.status(500).json({
                    ok: false,
                    message: 'Error en la petición de albums',
                    error,
                });
            }
            if (albums) {
                res.status(200).json({
                    ok: true,
                    albums,
                });
            } else {
                res.status(400).json({
                    ok: true,
                    message: 'No hay albums en la BBDD',
                });
            }
        });
    } else {
        //si hay parámetro de id de artista sacará los albums del artista
        Album.find({ artist: artistId }).populate({ path: 'artist' }).exec((error, albums) => {
            if (error) {
                return res.status(500).json({
                    ok: false,
                    message: 'Error en la petición de albums',
                    error,
                });
            }
            if (albums) {
                res.status(200).json({
                    ok: true,
                    albums,
                });
            } else {
                res.status(400).json({
                    ok: true,
                    message: 'No hay albums de este artista',
                });
            }
        });
    }
});

//actualizar album
albumRoutes.post('/update/:albumId', validateToken, (req: Request, res: Response) => {
    var albumId = req.params.albumId;

    //datos a actualizar
    var album = req.body;

    Album.findByIdAndUpdate(albumId, album, { new: true }, (error, albumUpdated) => {
        if (error) {
            return res.status(500).json({
                ok: false,
                message: 'Error al actualizar álbum',
                error,
            });
        }
        if (albumUpdated) {
            res.status(200).json({
                ok: true,
                message: 'Álbum actualizado con éxito',
                album: albumUpdated,
            });
        } else {
            res.status(400).json({
                ok: true,
                message: 'No se pudo actualizar el álbum',
            });
        }
    });

});

//eliminar álbum
albumRoutes.delete('/delete/:albumId', validateToken, (req: Request, res: Response) => {
    var albumId = req.params.albumId;

    Album.findByIdAndDelete(albumId)
        .then(albumDeleted => {
            Song.find({ album: albumDeleted._id }).remove();
            return res.status(200).json({
                ok: true,
                message: 'Álbum borrado con éxito',
            });
        })
        .catch(error => {
            res.json({
                ok: false,
                message: 'Error al eliminar el álbum',
                error,
            });
        });
});

//Subir imagen de álbum
albumRoutes.post('/upload-image/:albumId', [validateToken, md_upload], (req: any, res: Response) => {

    var albumId = req.params.albumId;
    if (req.files) {
        var file_path: string = req.files.image.path;
        var file_path_split = file_path.split('\\');
        var file_name = file_path_split[2];
        var file_name_split = file_name.split('\.');
        var file_extension = file_name_split[1];
        if (file_extension == 'jpeg' || file_extension == 'jpg' || file_extension == 'png' || file_extension == 'gif') {
            Album.findByIdAndUpdate(albumId, { image: file_name }, { new: true }, (error, albumUpdated) => {
                if (error) {
                    return res.status(500).json({
                        ok: false,
                        error,
                    });
                }
                if (albumUpdated) {
                    res.status(200).json({
                        ok: true,
                        message: 'Imagen del álbum subida con éxito',
                        album: albumUpdated,
                    });
                } else {
                    res.status(400).json({
                        ok: false,
                        message: 'No se pudo subir la imagen del álbum',
                    });
                }
            });
        } else {
            //si el formato no es correcto, borro el archivo
            fs.unlink(file_path, (err) => {
                if (err) {
                    console.log(err);
                }
            });
            return res.status(400).json({
                ok: false,
                message: 'El formato de la imagen no es válido',
            })
        }
    } else {
        return res.status(400).json({
            ok: false,
            message: 'No has subido ninguna imagen',
        });
    }
});




//Conseguir imagen de album
albumRoutes.get('/get-image/:albumId', (req: Request, res: Response) => {
    var albumId = req.params.albumId;
    Album.findById(albumId, (error, album) => {
        if (error) {
            return res.status(500).json({
                ok: false,
                message: 'Error en la petición de imagen de album',
                error,
            });
        }
        if (album) {
            var filePath = './uploads/albums/' + album.image;
            fs.exists(filePath, (exists) => {
                if (exists) {
                    return res.status(200).sendFile(path.resolve(filePath));
                } else {
                    res.status(400).json({
                        ok: false,
                        message: 'No existe imagen de álbum',
                    });
                }
            });
        } else {
            res.json({
                ok: false,
                message: 'No existe el álbum',
            });
        }
    });
});


//buscar en la aplicación cualquier coincidencia ya sea álbum,artista o canción

albumRoutes.get('/search/:searchString', validateToken, async (req: Request, res: Response) => {
    var searchString: string = req.params.searchString;
    var artists: Array<any>;
    var albums: Array<any>;
    var songs: Array<any>;
    var albumsInArtist: Array<any>;
    var songsInAlbums: Array<any>;
    try {

        //buscar las coincidencias directas de la palabra en artistas, álbums y canciones
        artists = await Artist.find({ 'name': { $regex: new RegExp(searchString, "i") } });
        albums = await Album.find({ 'title': { $regex: new RegExp(searchString, "i") } }).populate({path:'artist'});
        songs = await Song.find({ 'title': { $regex: new RegExp(searchString, "i") } }).populate({path:'album',populate: { path: 'artist', model: 'Artist' } });

        if(artists.length==0 && albums.length==0 && songs.length==0){
            return res.json({
                ok:false,
                message:"No se encontraron coincidencias",
            });
        }

        //buscar los albumes de los artistas encontrados
        for (let i = 0; i < artists.length; i++) {
            albumsInArtist = await Album.find({ 'artist': artists[i]._id }).populate({path:'artist'});
            albums = [...albums, ...albumsInArtist];
        }

        //buscar las canciones de los álbums encontrados
        for (let i = 0; i < albums.length; i++) {
            songsInAlbums = await Song.find({ 'album': albums[i]._id }).populate({path:'album',populate: { path: 'artist', model: 'Artist' } });
            songs = [...songs, ...songsInAlbums];
        }

        return res.json({
            ok:true,
            artists,
            albums,
            songs,
        })
    } catch (error) {
        console.log(error);
        return res.json({
            ok:false,
            message:"Error en la petición de búsqueda",
        });
    }
    
});