import {Schema,model} from 'mongoose';

var albumSchema=new Schema({
    title:{
        type:String,
        required:[true, 'El título del album es requerido'],
    },
    year:{
        type:Number,
    },
    image:{
        type:String,
        default:'default_album.jpg',
    },
    description:{
        type:String,
    },
    artist:{
        type:Schema.Types.ObjectId,
        ref:'Artist',
        required:[true,'Debe existir una referencia a un artista']
    }
});


export var Album=model('Album',albumSchema);