import express from 'express';

export class Server{
    public app:express.Application;
    public port:number; 

    constructor(){
        this.app=express();
        this.port=3000;
    }

    start(callback:Function){
        this.app.listen(this.port,callback());
    }
}