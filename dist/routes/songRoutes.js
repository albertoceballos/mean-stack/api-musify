"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var Song_1 = require("../models/Song");
//middleware autenticación
var validateToken_1 = require("../middlewares/validateToken");
//fileSystem
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
//multipart
var multipart = require('connect-multiparty');
var md_upload = multipart({ uploadDir: './uploads/songs' });
exports.songRoutes = express_1.Router();
//Crear nueva canción
exports.songRoutes.post('/create', validateToken_1.validateToken, function (req, res) {
    var song = {
        number: req.body.number || null,
        title: req.body.title,
        duration: req.body.duration || null,
        file: null,
        album: req.body.album,
    };
    Song_1.Song.create(song).then(function (songCreated) {
        if (songCreated) {
            res.status(200).json({
                ok: true,
                message: 'Nueva canción creada con éxito',
                song: songCreated,
            });
        }
        else {
            res.status(400).json({
                ok: false,
                message: 'No se pudo crear la nueva canción',
            });
        }
    })
        .catch(function (error) {
        res.status(500).json({
            ok: false,
            message: 'Error al crear nueva canción',
            error: error,
        });
    });
});
//conseguir una canción por su id
exports.songRoutes.get('/get/:songId', validateToken_1.validateToken, function (req, res) {
    var songId = req.params.songId;
    Song_1.Song.findById(songId).populate({ path: 'album' }).exec(function (error, song) {
        if (error) {
            return res.status(500).json({
                ok: false,
                message: 'Error en la petición de canción',
                error: error,
            });
        }
        if (song) {
            res.status(200).json({
                ok: true,
                song: song,
            });
        }
        else {
            res.status(400).json({
                ok: false,
                message: 'No existe la canción',
            });
        }
    });
});
//listado de canciones
exports.songRoutes.get('/getAll/:albumId?', validateToken_1.validateToken, function (req, res) {
    var albumId = req.params.albumId;
    //si no hay parametro de Id de album saca todas las canciones, sino las canciones del album del parámetro
    if (!albumId) {
        var find = Song_1.Song.find({}).sort('number');
    }
    else {
        var find = Song_1.Song.find({ album: albumId }).sort('number');
    }
    find.populate({ path: 'album', populate: { path: 'artist', model: 'Artist' } }).exec(function (error, songs) {
        if (error) {
            return res.status(500).json({
                ok: false,
                message: 'Error en la petición de álbums',
                error: error,
            });
        }
        if (songs) {
            res.status(200).json({
                ok: true,
                songs: songs,
            });
        }
        else {
            res.status(400).json({
                ok: false,
                message: 'No hay canciones',
            });
        }
    });
});
//actualizar canciones
exports.songRoutes.put('/update/:songId', validateToken_1.validateToken, function (req, res) {
    var songId = req.params.songId;
    var updateData = req.body;
    Song_1.Song.findByIdAndUpdate(songId, updateData, { new: true }, function (error, songUpdated) {
        if (error) {
            return res.status(500).json({
                ok: false,
                message: 'Error al actualizar canción',
                error: error,
            });
        }
        if (songUpdated) {
            res.status(200).json({
                ok: true,
                message: 'Canción actualizada con éxito',
                song: songUpdated,
            });
        }
        else {
            res.status(400).json({
                ok: false,
                message: 'No se pudo actualizar la canción',
            });
        }
    });
});
//eliminar una canción
exports.songRoutes.delete('/delete/:songId', validateToken_1.validateToken, function (req, res) {
    var songId = req.params.songId;
    Song_1.Song.findByIdAndDelete(songId, function (error, songDeleted) {
        if (error) {
            return res.status(500).json({
                ok: false,
                message: 'Error en la petición de borrar canción',
                error: error,
            });
        }
        if (songDeleted) {
            res.status(200).json({
                ok: true,
                message: 'Canción borrada con éxito',
                song: songDeleted,
            });
        }
        else {
            res.status(400).json({
                ok: true,
                message: 'No se pudo borrar la canción',
            });
        }
    });
});
//subir archivo de audio de la canción
exports.songRoutes.post('/upload-file/:songId', [validateToken_1.validateToken, md_upload], function (req, res) {
    var songId = req.params.songId;
    if (req.files) {
        var file_path = req.files.file.path;
        var file_path_split = file_path.split('\\');
        var file_name = file_path_split[2];
        var file_name_split = file_name.split('\.');
        var file_extension = file_name_split[1];
        if (file_extension == 'mp3' || file_extension == 'ogg') {
            Song_1.Song.findByIdAndUpdate(songId, { file: file_name }, function (error, songUpdated) {
                if (error) {
                    return res.status(500).json({
                        ok: false,
                        message: 'Error en la petición al actualizar la canción',
                        error: error,
                    });
                }
                if (songUpdated) {
                    res.status(200).json({
                        ok: true,
                        message: 'Archivo de audio subido con éxito',
                        song: songUpdated,
                    });
                }
                else {
                    res.status(400).json({
                        ok: false,
                        message: 'No se pudo actualizar la canción',
                    });
                }
            });
        }
        else {
            fs_1.default.unlink(file_path, function (err) {
                if (err) {
                    console.log(err);
                }
            });
            res.status(400).json({
                ok: false,
                message: 'El formato del archivo no es válido',
            });
        }
    }
    else {
        res.status(400).json({
            ok: false,
            message: 'No has subido ningún archivo',
        });
    }
});
//conseguir archivo de audio
exports.songRoutes.get('/get-file/:songFile', function (req, res) {
    var songFile = req.params.songFile;
    if (songFile) {
        var filePath = './uploads/songs/' + songFile;
        fs_1.default.exists(filePath, function (exists) {
            if (exists) {
                return res.status(200).sendFile(path_1.default.resolve(filePath));
            }
            else {
                res.status(400).json({
                    ok: false,
                    message: 'No existe el archivo de audio de la canción',
                });
            }
        });
    }
    else {
        res.status(400).json({
            ok: false,
            message: 'No existe la canción',
        });
    }
});
