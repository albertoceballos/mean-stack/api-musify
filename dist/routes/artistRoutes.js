"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
//modelos
var Artist_1 = require("../models/Artist");
var Album_1 = require("../models/Album");
var Song_1 = require("../models/Song");
//middleware autenticación
var validateToken_1 = require("../middlewares/validateToken");
//fileSystem
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
//multipart
var multipart = require('connect-multiparty');
var md_upload = multipart({ uploadDir: './uploads/artists' });
exports.artistRoutes = express_1.Router();
//crear un nuevo artista
exports.artistRoutes.post('/create', validateToken_1.validateToken, function (req, res) {
    var artist = {
        name: req.body.name,
        description: req.body.description,
        image: req.body.image || 'default_artist.jpg',
    };
    if (!artist.name) {
        return res.status(400).json({
            ok: false,
            message: 'El nombre del artista es obligaorio',
        });
    }
    Artist_1.Artist.create(artist).then(function (artistCreated) {
        if (artistCreated) {
            res.status(200).json({
                ok: true,
                message: 'nuevo artista creado con éxito',
                artist: artistCreated,
            });
        }
        else {
            res.status(400).json({
                ok: false,
                message: 'Error al crear nuevo artista',
            });
        }
    })
        .catch(function (error) {
        res.status(500).json({
            ok: false,
            error: error,
        });
    });
});
//conseguir artista por Id
exports.artistRoutes.get('/get/:artistId', function (req, res) {
    var artistId = req.params.artistId;
    if (!artistId) {
        return res.status(400).json({
            ok: false,
            message: 'Falta el id del artista',
        });
    }
    Artist_1.Artist.findById(artistId, function (err, artist) {
        if (err) {
            return res.status(500).json({
                ok: false,
                message: 'Error en la petición de artista',
            });
        }
        if (artist) {
            res.status(200).json({
                ok: true,
                artist: artist,
            });
        }
        else {
            res.status(400).json({
                ok: false,
                message: 'No existe el artista',
            });
        }
    });
});
//listado paginado de artistas
exports.artistRoutes.get('/getAll/:page', function (req, res) {
    var page = req.params.page;
    var itemsPerPage = 5;
    Artist_1.Artist.paginate({}, { sort: 'name', page: page, perPage: itemsPerPage }, function (err, result) {
        if (err) {
            return res.status(500).json({
                ok: false,
                message: 'Error en la petición',
            });
        }
        if (result) {
            return res.status(200).json({
                ok: true,
                result: result,
            });
        }
        else {
            return res.status(400).json({
                ok: true,
                error: 'No hay resultados',
            });
        }
    });
});
//actualizar artista
exports.artistRoutes.post('/update/:artistId', validateToken_1.validateToken, function (req, res) {
    var artistId = req.params.artistId;
    var artist = req.body;
    Artist_1.Artist.findByIdAndUpdate(artistId, artist, { new: true }, function (error, artistUpdated) {
        if (error) {
            return res.status(500).json({
                ok: false,
                message: 'Error al actualizar artista',
                error: error,
            });
        }
        if (artistUpdated) {
            res.status(200).json({
                ok: true,
                message: 'Artista actualizado con éxito',
                artist: artistUpdated,
            });
        }
        else {
            res.status(400).json({
                ok: false,
                message: 'No se puede actualizar el artista, parece que no existe',
            });
        }
    });
});
//Eliminar un artista
exports.artistRoutes.delete('/delete/:artistId', [validateToken_1.validateToken, md_upload], function (req, res) {
    var artistId = req.params.artistId;
    //borramos el arista y después los álbumes y canciiones relacionas mediante promesas
    Artist_1.Artist.findByIdAndDelete(artistId)
        .then(function (artistDeleted) {
        Album_1.Album.find({ artist: artistDeleted._id }).remove()
            .then(function (albumDeleted) {
            Song_1.Song.find({ album: albumDeleted._id }).remove();
            return res.status(200).json({
                ok: true,
                message: 'Artista borrado con éxito',
            });
        });
    }).catch(function (error) {
        return res.status(400).json({
            ok: false,
            error: error,
        });
    });
});
//Subir imagen de artista
exports.artistRoutes.post('/upload-image/:artistId', [validateToken_1.validateToken, md_upload], function (req, res) {
    var artistId = req.params.artistId;
    if (req.files) {
        var file_path = req.files.image.path;
        var file_path_split = file_path.split('\\');
        console.log(file_path);
        var file_name = file_path_split[2];
        var file_name_split = file_name.split('\.');
        var file_extension = file_name_split[1];
        if (file_extension == 'jpg' || file_extension == 'jpeg' || file_extension == 'png' || file_extension == 'gif') {
            Artist_1.Artist.findByIdAndUpdate(artistId, { image: file_name }, { new: true }, function (error, artistUpdated) {
                if (error) {
                    return res.status(500).json({
                        ok: false,
                        error: error,
                    });
                }
                if (artistUpdated) {
                    return res.status(200).json({
                        ok: true,
                        message: 'Imagen subida con éxito',
                        artista: artistUpdated,
                    });
                }
                else {
                    return res.status(400).json({
                        ok: false,
                        message: 'Error al actualizar el artista',
                    });
                }
            });
        }
        else {
            fs_1.default.unlink(file_path, function (err) {
                if (err) {
                    console.log(err);
                }
            });
            return res.status(400).json({
                ok: false,
                message: 'El formato del archivo de imagen no es correcto',
            });
        }
    }
    else {
        res.status(404).json({
            ok: false,
            message: 'no has subido ninguna imagen',
        });
    }
});
//conseguir imagen de artista con su Id
exports.artistRoutes.get('/get-image/:artistId', function (req, res) {
    var artistId = req.params.artistId;
    Artist_1.Artist.findById(artistId, function (err, artist) {
        if (err) {
            return res.status(500).json({
                ok: false,
                message: 'Error en la petición de imagen',
            });
        }
        if (artist) {
            var pathFile = './uploads/artists/' + artist.image;
            fs_1.default.exists(pathFile, function (exists) {
                if (exists) {
                    return res.status(200).sendFile(path_1.default.resolve(pathFile));
                }
                else {
                    return res.status(404).json({
                        ok: false,
                        message: 'No existe imagen de artista',
                    });
                }
            });
        }
        else {
            res.status(400).json({
                ok: true,
                message: 'No existe el artista',
            });
        }
    });
});
