"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var bcrypt_nodejs_1 = __importDefault(require("bcrypt-nodejs"));
var User_1 = require("../models/User");
var JWT_1 = require("../services/JWT");
var validateToken_1 = require("../middlewares/validateToken");
//multipart
var multipart = require('connect-multiparty');
var md_upload = multipart({ uploadDir: './uploads/users' });
//fileSystem
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
exports.userRoutes = express_1.Router();
//crear nuevo Usuario
exports.userRoutes.post('/create', function (req, res) {
    var user = {
        role: req.body.role || 'ROLE_USER',
        name: req.body.name,
        surname: req.body.surname,
        email: req.body.email,
        password: req.body.password,
        image: req.body.image,
    };
    if (!user.name || !user.email || !user.password) {
        return res.status(400).json({
            ok: false,
            message: 'Rellena todos los campos obligatorios',
        });
    }
    //encriptar contraseña
    user.password = bcrypt_nodejs_1.default.hashSync(user.password);
    //crear usuario
    User_1.User.create(user).then(function (userCreated) {
        if (userCreated) {
            res.status(200).json({
                ok: true,
                user: userCreated,
                message: 'Nuevo usuario creado con éxito',
            });
        }
        else {
            res.status(400).json({
                ok: false,
                message: 'Error al crear nuevo usuario',
            });
        }
    })
        .catch(function (err) {
        return res.status(400).json({
            ok: false,
            err: err,
        });
    });
});
//login de usuario
exports.userRoutes.post('/login', function (req, res) {
    var email = req.body.email;
    var password = req.body.password;
    var getHash = req.body.getHash;
    if (!email || !password) {
        return res.status(400).json({
            ok: false,
            message: 'Faltan datos',
        });
    }
    User_1.User.findOne({ email: email }, function (err, user) {
        if (err) {
            return res.status(500).json({
                ok: false,
                message: 'Error en el login',
                error: err,
            });
        }
        //si existe el email:
        if (user) {
            //comparamos las contraseñas;
            var validatePassword = bcrypt_nodejs_1.default.compareSync(password, user.password);
            if (validatePassword) {
                //crear el token con el servicio
                var token = JWT_1.JWT.createToken(user);
                //devolver el token si se pide en el cuerpo
                if (getHash) {
                    return res.status(200).json({
                        ok: true,
                        token: token
                    });
                    //si no devolver solo el usuario
                }
                return res.status(200).json({
                    ok: true,
                    message: 'Login correcto',
                    user: user,
                });
            }
            else {
                res.status(400).json({
                    ok: false,
                    message: 'La contraseña no es correcta',
                });
            }
        }
        else {
            res.status(400).json({
                ok: false,
                message: 'El usuario no existe',
            });
        }
    });
});
//actualizar usuario
exports.userRoutes.put('/update', validateToken_1.validateToken, function (req, res) {
    var idUsuario = req.user.sub;
    var user = {
        role: req.body.role || 'ROLE_USER',
        name: req.body.name,
        surname: req.body.surname,
        email: req.body.email,
        image: req.body.image,
    };
    if (!user.name || !user.email) {
        return res.status(400).json({
            ok: false,
            message: 'Rellena todos los campos obligatorios',
        });
    }
    User_1.User.findByIdAndUpdate(idUsuario, user, { new: true }, function (error, userUpdated) {
        if (error) {
            return res.status(500).json({
                ok: false,
                error: error,
                message: 'Error al actualizar Usuario',
            });
        }
        if (userUpdated) {
            res.status(200).json({
                ok: true,
                message: 'Usuario actualizado con éxito',
                user: userUpdated,
            });
        }
        else {
            res.status(400).json({
                ok: false,
                message: 'No se pudo actualizar el usuario',
            });
        }
    });
});
//subir imagen de usuario
exports.userRoutes.post('/upload-avatar', [validateToken_1.validateToken, md_upload], function (req, res) {
    var idUsuario = req.user.sub;
    if (req.files) {
        var file_path = req.files.imagen.path;
        var fileSplit = file_path.split('\\');
        console.log(fileSplit);
        var fileName = fileSplit[2];
        var fileSplitExtension = fileName.split('\.');
        var fileExtension = fileSplitExtension[1];
        console.log(fileExtension);
        if (fileExtension == 'jpg' || fileExtension == 'jpeg' || fileExtension == 'png' || fileExtension == 'gif') {
            User_1.User.findByIdAndUpdate(idUsuario, { image: fileName }, { new: true }, function (error, userUpdated) {
                if (error) {
                    return res.status(500).json({
                        ok: false,
                        error: error,
                        message: 'Error al actualizar Usuario',
                    });
                }
                if (userUpdated) {
                    res.status(200).json({
                        ok: true,
                        message: 'Usuario actualizado con éxito',
                        image: fileName,
                        user: userUpdated,
                    });
                }
                else {
                    res.status(400).json({
                        ok: false,
                        message: 'No se pudo actualizar el usuario',
                    });
                }
            });
        }
        else {
            fs_1.default.unlink(file_path, function (err) {
                if (err) {
                    console.log(err);
                }
            });
            res.status(400).json({
                ok: false,
                message: 'El formato de la imagen no es válido',
            });
        }
    }
    else {
        return res.status(400).json({
            ok: false,
            message: 'No has subido ninguna imagen',
        });
    }
});
exports.userRoutes.get('/get-image-user/:image', function (req, res) {
    var imageFile = req.params.image;
    var pathFile = './uploads/users/' + imageFile;
    fs_1.default.exists(pathFile, function (exists) {
        if (exists) {
            res.sendFile(path_1.default.resolve(pathFile));
        }
        else {
            res.status(200).json({
                message: 'No existe la imagen',
            });
        }
    });
});
