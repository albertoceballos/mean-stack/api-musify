"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
//modelos
var Artist_1 = require("../models/Artist");
var Album_1 = require("../models/Album");
var Song_1 = require("../models/Song");
//middleware autenticación
var validateToken_1 = require("../middlewares/validateToken");
//fileSystem
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
//multipart
var multipart = require('connect-multiparty');
var md_upload = multipart({ uploadDir: './uploads/albums' });
exports.albumRoutes = express_1.Router();
//Crear nuevo álbum
exports.albumRoutes.post('/create', validateToken_1.validateToken, function (req, res) {
    var album = {
        title: req.body.title,
        year: req.body.year || null,
        image: req.body.image || 'default_album.jpg',
        description: req.body.description || null,
        artist: req.body.artist,
    };
    if (!album.title || !album.artist) {
        return res.status(400).json({
            ok: false,
            message: 'Faltan datos, rellena los campos obligatorios',
        });
    }
    Album_1.Album.create(album).then(function (albumCreated) {
        if (albumCreated) {
            res.status(200).json({
                ok: true,
                message: 'Nuevo album creado con éxito',
                album: albumCreated,
            });
        }
        else {
            res.status(500).json({
                ok: false,
                message: 'Error al crear nuevo álbum',
            });
        }
    })
        .catch(function (error) {
        res.status(500).json({
            ok: false,
            error: error,
        });
    });
});
//Conseguir un álbum por su id
exports.albumRoutes.get('/get/:albumId', function (req, res) {
    var albumId = req.params.albumId;
    Album_1.Album.findById(albumId).populate({ path: 'artist' }).exec(function (error, album) {
        if (error) {
            return res.status(500).json({
                ok: false,
                message: 'Error en la petición de álbum',
                error: error,
            });
        }
        if (album) {
            res.status(200).json({
                ok: true,
                album: album,
            });
        }
        else {
            res.status(400).json({
                ok: false,
                message: 'No existe el álbum',
            });
        }
    });
});
//Conseguir albums
exports.albumRoutes.get('/getAll/:artistId?', validateToken_1.validateToken, function (req, res) {
    var artistId = req.params.artistId;
    if (!artistId) {
        //si no hay parametro de id artista sacará todos los albums
        Album_1.Album.find({}).populate({ path: 'artist' }).exec(function (error, albums) {
            if (error) {
                return res.status(500).json({
                    ok: false,
                    message: 'Error en la petición de albums',
                    error: error,
                });
            }
            if (albums) {
                res.status(200).json({
                    ok: true,
                    albums: albums,
                });
            }
            else {
                res.status(400).json({
                    ok: true,
                    message: 'No hay albums en la BBDD',
                });
            }
        });
    }
    else {
        //si hay parámetro de id de artista sacará los albums del artista
        Album_1.Album.find({ artist: artistId }).populate({ path: 'artist' }).exec(function (error, albums) {
            if (error) {
                return res.status(500).json({
                    ok: false,
                    message: 'Error en la petición de albums',
                    error: error,
                });
            }
            if (albums) {
                res.status(200).json({
                    ok: true,
                    albums: albums,
                });
            }
            else {
                res.status(400).json({
                    ok: true,
                    message: 'No hay albums de este artista',
                });
            }
        });
    }
});
//actualizar album
exports.albumRoutes.post('/update/:albumId', validateToken_1.validateToken, function (req, res) {
    var albumId = req.params.albumId;
    //datos a actualizar
    var album = req.body;
    Album_1.Album.findByIdAndUpdate(albumId, album, { new: true }, function (error, albumUpdated) {
        if (error) {
            return res.status(500).json({
                ok: false,
                message: 'Error al actualizar álbum',
                error: error,
            });
        }
        if (albumUpdated) {
            res.status(200).json({
                ok: true,
                message: 'Álbum actualizado con éxito',
                album: albumUpdated,
            });
        }
        else {
            res.status(400).json({
                ok: true,
                message: 'No se pudo actualizar el álbum',
            });
        }
    });
});
//eliminar álbum
exports.albumRoutes.delete('/delete/:albumId', validateToken_1.validateToken, function (req, res) {
    var albumId = req.params.albumId;
    Album_1.Album.findByIdAndDelete(albumId)
        .then(function (albumDeleted) {
        Song_1.Song.find({ album: albumDeleted._id }).remove();
        return res.status(200).json({
            ok: true,
            message: 'Álbum borrado con éxito',
        });
    })
        .catch(function (error) {
        res.json({
            ok: false,
            message: 'Error al eliminar el álbum',
            error: error,
        });
    });
});
//Subir imagen de álbum
exports.albumRoutes.post('/upload-image/:albumId', [validateToken_1.validateToken, md_upload], function (req, res) {
    var albumId = req.params.albumId;
    if (req.files) {
        var file_path = req.files.image.path;
        var file_path_split = file_path.split('\\');
        var file_name = file_path_split[2];
        var file_name_split = file_name.split('\.');
        var file_extension = file_name_split[1];
        if (file_extension == 'jpeg' || file_extension == 'jpg' || file_extension == 'png' || file_extension == 'gif') {
            Album_1.Album.findByIdAndUpdate(albumId, { image: file_name }, { new: true }, function (error, albumUpdated) {
                if (error) {
                    return res.status(500).json({
                        ok: false,
                        error: error,
                    });
                }
                if (albumUpdated) {
                    res.status(200).json({
                        ok: true,
                        message: 'Imagen del álbum subida con éxito',
                        album: albumUpdated,
                    });
                }
                else {
                    res.status(400).json({
                        ok: false,
                        message: 'No se pudo subir la imagen del álbum',
                    });
                }
            });
        }
        else {
            //si el formato no es correcto, borro el archivo
            fs_1.default.unlink(file_path, function (err) {
                if (err) {
                    console.log(err);
                }
            });
            return res.status(400).json({
                ok: false,
                message: 'El formato de la imagen no es válido',
            });
        }
    }
    else {
        return res.status(400).json({
            ok: false,
            message: 'No has subido ninguna imagen',
        });
    }
});
//Conseguir imagen de album
exports.albumRoutes.get('/get-image/:albumId', function (req, res) {
    var albumId = req.params.albumId;
    Album_1.Album.findById(albumId, function (error, album) {
        if (error) {
            return res.status(500).json({
                ok: false,
                message: 'Error en la petición de imagen de album',
                error: error,
            });
        }
        if (album) {
            var filePath = './uploads/albums/' + album.image;
            fs_1.default.exists(filePath, function (exists) {
                if (exists) {
                    return res.status(200).sendFile(path_1.default.resolve(filePath));
                }
                else {
                    res.status(400).json({
                        ok: false,
                        message: 'No existe imagen de álbum',
                    });
                }
            });
        }
        else {
            res.json({
                ok: false,
                message: 'No existe el álbum',
            });
        }
    });
});
//buscar en la aplicación cualquier coincidencia ya sea álbum,artista o canción
exports.albumRoutes.get('/search/:searchString', validateToken_1.validateToken, function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var searchString, artists, albums, songs, albumsInArtist, songsInAlbums, i, i, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                searchString = req.params.searchString;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 13, , 14]);
                return [4 /*yield*/, Artist_1.Artist.find({ 'name': { $regex: new RegExp(searchString, "i") } })];
            case 2:
                //buscar las coincidencias directas de la palabra en artistas, álbums y canciones
                artists = _a.sent();
                return [4 /*yield*/, Album_1.Album.find({ 'title': { $regex: new RegExp(searchString, "i") } }).populate({ path: 'artist' })];
            case 3:
                albums = _a.sent();
                return [4 /*yield*/, Song_1.Song.find({ 'title': { $regex: new RegExp(searchString, "i") } }).populate({ path: 'album', populate: { path: 'artist', model: 'Artist' } })];
            case 4:
                songs = _a.sent();
                if (artists.length == 0 && albums.length == 0 && songs.length == 0) {
                    return [2 /*return*/, res.json({
                            ok: false,
                            message: "No se encontraron coincidencias",
                        })];
                }
                i = 0;
                _a.label = 5;
            case 5:
                if (!(i < artists.length)) return [3 /*break*/, 8];
                return [4 /*yield*/, Album_1.Album.find({ 'artist': artists[i]._id }).populate({ path: 'artist' })];
            case 6:
                albumsInArtist = _a.sent();
                albums = albums.concat(albumsInArtist);
                _a.label = 7;
            case 7:
                i++;
                return [3 /*break*/, 5];
            case 8:
                i = 0;
                _a.label = 9;
            case 9:
                if (!(i < albums.length)) return [3 /*break*/, 12];
                return [4 /*yield*/, Song_1.Song.find({ 'album': albums[i]._id }).populate({ path: 'album', populate: { path: 'artist', model: 'Artist' } })];
            case 10:
                songsInAlbums = _a.sent();
                songs = songs.concat(songsInAlbums);
                _a.label = 11;
            case 11:
                i++;
                return [3 /*break*/, 9];
            case 12: return [2 /*return*/, res.json({
                    ok: true,
                    artists: artists,
                    albums: albums,
                    songs: songs,
                })];
            case 13:
                error_1 = _a.sent();
                console.log(error_1);
                return [2 /*return*/, res.json({
                        ok: false,
                        message: "Error en la petición de búsqueda",
                    })];
            case 14: return [2 /*return*/];
        }
    });
}); });
