"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var albumSchema = new mongoose_1.Schema({
    title: {
        type: String,
        required: [true, 'El título del album es requerido'],
    },
    year: {
        type: Number,
    },
    image: {
        type: String,
        default: 'default_album.jpg',
    },
    description: {
        type: String,
    },
    artist: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Artist',
        required: [true, 'Debe existir una referencia a un artista']
    }
});
exports.Album = mongoose_1.model('Album', albumSchema);
