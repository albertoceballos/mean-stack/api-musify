"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var jwt_simple_1 = __importDefault(require("jwt-simple"));
var moment_1 = __importDefault(require("moment"));
var JWT = /** @class */ (function () {
    function JWT() {
    }
    JWT.createToken = function (user) {
        var payload = {
            sub: user._id,
            name: user.name,
            surname: user.surname,
            email: user.email,
            role: user.role,
            image: user.image,
            iat: moment_1.default().unix(),
            exp: moment_1.default().add(30, 'days').unix,
        };
        return jwt_simple_1.default.encode(payload, this.semilla);
    };
    JWT.semilla = "Una-clave-ultrasecreta";
    return JWT;
}());
exports.JWT = JWT;
